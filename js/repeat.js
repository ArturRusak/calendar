var Repeat = (function (Calendar) {

  var DAY = 24 * 60 * 60 * 1000;
  var
    MESSAGE_ERROR_MAX_LENGTH_AND_VAL_DAYS_ARR = 'Max length of days array is 7 and values from 0 to 7!',
    MESSAGE_ERROR_EMPTY_DAYS_ARR = 'The days array is empty!',
    MESSAGE_INFO_EVENT_NOT_FOUND = 'Event for repeat not found!',
    MESSAGE_ERROR_INVALID_INPUT_DATA = 'Invalid input data!',
    MESSAGE_ERROR_MUST_BE_ARRAY = 'The days must be in array!',
    MESSAGE_ERROR_ID_MUST_BE_NUMBER = 'ID must be a number!',
    MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION = 'The callback must be a function!';

  /**
   * Run of event callbacks from array
   */
  var runCallbacksRepeatsEvents = function (callbackList) {
    callbackList.forEach(function (callbackItem) {
      callbackItem();
    });
  };

  /**
   * Is Array
   * @param {Array} arr
   * @returns {boolean}
   */
  function isValidArray(arr) {
    return Object.prototype.toString.call(arr) === '[object Array]'
  }

  /**
   * Validate the array of days
   * @param {Array} days
   * @returns {boolean}
   */
  function checkArrayDays(days) {
    if (!isValidArray(days)) {
      console.error(MESSAGE_ERROR_MUST_BE_ARRAY);
      return false;
    }

    if (!days.length) {
      console.error(MESSAGE_ERROR_EMPTY_DAYS_ARR);
      return false;
    }

    for (var i = 0; i < days.length; i++) {
      if ((i > 6) ||(days[i] < 0) || (days[i] > 7)) {
        console.error(MESSAGE_ERROR_MAX_LENGTH_AND_VAL_DAYS_ARR);
        return false;
      }
    }
    return true;
  }

  /**
   * Find of closest day by number
   * @param {Array} days - Array of week days 1-7
   * @return {*}
   */
  function findClosestDay(days) {
    var currentDate = new Date();
    var currentDay = new Date().getDay();
    var closestEventDay = null;
    currentDay = currentDay === 0 ? 7 : currentDay; // sunday - 7 number
    days.sort();

    for (var i = 0; i < days.length; i++) {
      if (!closestEventDay && currentDay < days[i]) {
        closestEventDay = days[i];
      }
    }

    if (closestEventDay) {
      return currentDate.setDate(currentDate.getDate() + (closestEventDay - currentDay));
    }
    closestEventDay = days[0];

    return currentDate.setDate(currentDate.getDate() - currentDay + closestEventDay + 7);
  }

  /**
   * Handle of events schedule every day or by week days
   * @param {Array} days - days of week 1 - 7
   * @param {String} name - Name of event
   * @returns {function(): (*|string)} - Create new event instead modify
   */
  function handleRepeatsSchedule(days, name,) {

    if (days) {
      return function () {
        var stringDate = new Date(findClosestDay(days)).toISOString();
        return Calendar.addEvent(name, stringDate, runCallbacksRepeatsEvents);
      };
    }

    return function () {
      var stringDate = new Date(Date.now() + DAY).toISOString();
      return Calendar.addEvent(name, stringDate, runCallbacksRepeatsEvents);
    };
  }

  /**
   * Search the event item by id
   * @param {Number} id
   * @returns {*}
   */
  function searchEventByID(id) {
    var allEventsList = Calendar.getAllEvents();
    if (Array.isArray(allEventsList)){
      for (var i = 0; i < allEventsList.length; i++) {
        if (allEventsList[i].id === id) {
          return allEventsList[i];
        }
      }
    }
    return null;
  }

  /**
   * Create of repeat events
   * @param {String} name - Name of event
   * @param {String} date - mm/dd/yy hh:mm:ss
   * @param {Function} callback
   * @param {Array} days - array of days for repeat events, with values 1 - 7
   * @returns {*}
   */
  Calendar.addRepeatsEvent = function(name, date, callback, days) {

    if (!name || !date || !callback) {
      console.error(MESSAGE_ERROR_INVALID_INPUT_DATA);
      return;
    }

    var isFunction = this.isFunction(callback);
    if (!isFunction) {
      return console.error(MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION);
    }

    if (days && !checkArrayDays(days)) {
      return console.error(MESSAGE_ERROR_MAX_LENGTH_AND_VAL_DAYS_ARR);
    }

    var callbackList = [].concat(callback, handleRepeatsSchedule(days, name));
    return Calendar.addEvent(name, date, function () {
      return runCallbacksRepeatsEvents(callbackList);
    });
  };

  /**
   * Add repeats for event that already exist
   * @param {Number} id - Unique of event
   * @param {Array} days - array of days for repeat events, with values 1 - 7
   */
  Calendar.addRepeatsEventByID = function(id, days) {

    id = Calendar.validationID(id);
    if (!id) {
      console.error(MESSAGE_ERROR_INVALID_INPUT_DATA);
      return console.log(MESSAGE_ERROR_ID_MUST_BE_NUMBER);
    }

    if (days && !checkArrayDays(days)) {
      return console.error(MESSAGE_ERROR_MAX_LENGTH_AND_VAL_DAYS_ARR);
    }

    var event = searchEventByID(id);
    if (event) {
      callbackList = [].concat(event.callback, handleRepeatsSchedule(days, event.name, callbackList));
      Calendar.removeEvent(id);

      return Calendar.addEvent(event.name, event.date, function () {
        return runCallbacksRepeatsEvents(callbackList)
      });
    }

    console.log(MESSAGE_INFO_EVENT_NOT_FOUND);
  };

  return Calendar;
})(Calendar);