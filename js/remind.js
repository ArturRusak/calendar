var Remind = (function (Calendar) {

  var
    DAYS = 'd',
    HOURS = 'h',
    MINUTES = 'm',
    DAY = 24 * 60 * 60 * 1000,
    HOUR = 60 * 60 * 1000,
    MINUTE = 60 * 1000;

  var
    MESSAGE_INFO_FOR_FLAGS = 'For input use(\'d\', \'h\', \'m\')!',
    MESSAGE_INFO_EVENT_NOT_FOUND = 'Event for remind not found!',
    MESSAGE_ERROR_SET_LESS_TIME = 'Set less time to event!',
    MESSAGE_ERROR_INVALID_INPUT_DATA = 'Invalid input data!',
    MESSAGE_ERROR_EVENTS_FOR_REMIND_NOT_FOUND = 'Events for remind not found!',
    MESSAGE_ERROR_ID_MUST_BE_NUMBER = 'ID must be a number!',
    MESSAGE_ERROR_TIME_MUST_BE_NUMBER = 'Time value must be a number!',
    MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION = 'The callback must be a function!',
    MESSAGE_ERROR_TIME_FLAGS_EXCEPTION = 'Time flag must be a string with length one symbol,' +
      ' and may have values \'d\', \'h\', \'m\'';

  /**
   * Check the case with flags for one event
   * @param {Object} event
   * @param {Number} timeValue
   * @param {String} timeFlag
   * @param {Function} callback
   * @returns {*}
   */
  function checkTimeFlagToEvent(event, timeValue, timeFlag, callback) {
    if (timeFlag === DAYS) {
      event = setReminderToEvent(event, timeValue * DAY, callback);

    } else if (timeFlag === HOURS){
      event = setReminderToEvent(event, timeValue * HOUR, callback);

    } else if (timeFlag === MINUTES){
      event = setReminderToEvent(event, timeValue * MINUTE, callback);

    } else {
      return false;
    }
    return event;
  }

  /**
   * Check the case with flags for all events
   * @param {Number} timeValue
   * @param {String} timeFlag
   * @param {Function} callback
   * @returns {*}
   */
  function checkTimeFlagToAllEvents(timeValue, timeFlag, callback) {
    var eventsList = [];
    if (timeFlag === DAYS) {
      eventsList = setReminderToAllEvents(timeValue * DAY, callback);

    } else if (timeFlag === HOURS){
      eventsList = setReminderToAllEvents(timeValue * HOUR, callback);

    } else if (timeFlag === MINUTES){
      eventsList = setReminderToAllEvents(timeValue * MINUTE, callback);

    } else {
      return null;
    }
    return eventsList
  }

  /**
   * Validation of remind data
   * @param {Number} timeValue
   * @param {String} timeFlag
   * @param {Function} callback
   * @returns {boolean}
   */
  function isValidRemindData(timeValue, timeFlag, callback) {

    if (typeof timeValue !== 'number') {
      console.error(MESSAGE_ERROR_TIME_MUST_BE_NUMBER);
      return false;
    }

    if (typeof timeFlag !== 'string' || timeFlag.length > 1) {
      console.error(MESSAGE_ERROR_TIME_FLAGS_EXCEPTION);
      return false;
    }

    var isFunction = Calendar.isFunction(callback);
    if (!isFunction) {
      console.error(MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION);
      return false;
    }

    return true;
  }

  /**
   * Search event by ID
   * @param {Number} id
   * @returns {*}
   */
  function searchEventByID(id) {
    var allEventsList = Calendar.getAllEvents();
    for (var i = 0; i < allEventsList.length; i++) {
      if (allEventsList[i].id === id) {
        return allEventsList[i];
      }
    }
    return null;
  }

  /**
   * Run array of callbacks
   * @param {Object} event
   * @param {Function} callback
   * @returns {Function}
   */
  function runCallbacksRemindEvents(event, callback) {
    var callbackList = [];
    callbackList.push(callback, setDefaultEvent(event));
    return function () {
      callbackList.forEach(function (callbackItem) {
        callbackItem();
      });
    }
  }

  /**
   * Set Reminder for one event
   * @param {Object} event
   * @param {Number} timeToRemind
   * @param {Function} callback
   * @returns {*}
   */
  function setReminderToEvent(event, timeToRemind, callback) {
    var currentTime = Date.now();
    var remindTime = new Date(event.date);

    if (currentTime - remindTime > 0) {

      return console.error(MESSAGE_ERROR_SET_LESS_TIME);
    }
    remindTime.setTime(remindTime.getTime() - timeToRemind);
    var remindEventHandler = runCallbacksRemindEvents(event, callback);

    Calendar.removeEvent(event.id);
    return Calendar.addEvent(event.name , remindTime, remindEventHandler);
  }

  /**
   * Set Reminder for all events
   * @param {Number} timeToRemind
   * @param {Function} callback
   * @returns {*}
   */
  function setReminderToAllEvents(timeToRemind, callback) {
    var eventsList = Calendar.getAllEvents();

    if (Array.isArray(eventsList)) {
      eventsList.forEach(function (event) {
        setReminderToEvent(event, timeToRemind, callback);
      });
      return eventsList;
    }
    console.log(MESSAGE_INFO_EVENT_NOT_FOUND);
  }

  /**
   * Add the default event
   * @param {Object} event
   * @returns {Function}
   */
  function setDefaultEvent(event) {
    return function () {
      Calendar.addEvent(event.name, event.date, event.callback);
    };
  }

  /**
   * Set reminder by ID
   * @param {Number} id
   * @param {Number} timeValue
   * @param {String} timeFlag
   * @param {Function} callback
   * @returns {*}
   */
  Calendar.setReminderByID = function (id, timeValue, timeFlag, callback) {

    if (!id || !timeValue || !timeFlag || !callback) {
      return console.error(MESSAGE_ERROR_INVALID_INPUT_DATA);
    }

    if (!isValidRemindData(timeValue, timeFlag, callback)) {
      return MESSAGE_ERROR_INVALID_INPUT_DATA; //todo fix
    }

    id = Calendar.validationID(id);
    if (!id) {
      return console.error(MESSAGE_ERROR_ID_MUST_BE_NUMBER);
    }

    var event = searchEventByID(id);
    if (!event){
      return console.log(MESSAGE_INFO_EVENT_NOT_FOUND);
    }

    event = checkTimeFlagToEvent(event, timeValue, timeFlag, callback);
    if (!event) {
      return console.error(MESSAGE_INFO_FOR_FLAGS);
    }
    return event;
  };

  /**
   * Set of reminder for all events
   * @param {Number} timeValue
   * @param {String} timeFlag
   * @param {Function} callback
   * @return {*}
   */
  Calendar.setGlobalReminder = function (timeValue, timeFlag, callback) {

    if (!timeValue || !timeFlag || !callback) {
      return console.error(MESSAGE_ERROR_INVALID_INPUT_DATA);
    }

    if (!isValidRemindData(timeValue, timeFlag, callback)) {
      return console.error(MESSAGE_ERROR_INVALID_INPUT_DATA); //todo fix
    }

    var eventsList = checkTimeFlagToAllEvents(timeValue, timeFlag, callback );
    if (!eventsList){
      return console.error(MESSAGE_INFO_FOR_FLAGS);
    }
    if (eventsList.length) {
      return eventsList;
    }
    console.log(MESSAGE_ERROR_EVENTS_FOR_REMIND_NOT_FOUND);
  };

  return Calendar;
})(Calendar);
