(function() {
  var Calendar = (function () {

    var eventsList = []; //events list
    var timer = null; // timer of app
    var idInterval = 10000000; //
    var
      CALENDAR_EVENTS = 'Calendar_Events',
      MESSAGE_INFO_INVALID_OF_INPUT_DATA = 'Invalid of input data!',
      MESSAGE_INFO_EVENT_NOT_FOUND = 'ID not found',
      MESSAGE_INFO_OF_FLAGS = 'For input use(\'by month\', \'by day\' + date, \'by week\', \'between dates\' + dates)!',
      MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION = 'The callback must be a function!',
      MESSAGE_INFO_EVENTS_LIST_IS_EMPTY = 'Events list is empty!',
      MESSAGE_ERROR_INPUT_OF_ID = 'Please input ID of event',
      MESSAGE_ERROR_ID_MUST_BE_NUMBER = 'ID must be a number',
      MESSAGE_ERROR_THE_DATE_MUST_BE_VALID = 'Please input future date!',
      MESSAGE_ERROR_FORMAT_DATE = 'The date must be string in format mm/dd/yy hh:mm:ss';


    function Calendar() {
      eventsList = reviveEvents() || [];
    }

    /**
     * Generation of ID
     * @return {number}
     */
    function generateId() {
      return Math.floor((Math.random() * idInterval) + 1);
    }

    /**
     * Save the events in localstorage
     */
    function saveInStorage() {
      var readyToStoreEvents = JSON.stringify(eventsList, function (key, value) {
        if (typeof value === "function") {
          return value.toString();
        }
        return value;
      });
      localStorage.setItem(CALENDAR_EVENTS, readyToStoreEvents);
    }

    /**
     * Get Actual Array from storage
     * @param readyToUseEvents
     * @returns {Array}
     */
    function getActualEvents(readyToUseEvents) {
      var actualListEvents = [];
      readyToUseEvents.forEach(function (event) {
        if (event.date.getTime() - Date.now() > 0) {
          actualListEvents.push(event);
        }
      });
      return actualListEvents;
    }

    /**
     * Get events from storage
     */
    function reviveEvents(){
      var DATE = 'date';
      var CALLBACK = 'callback';
      var KOSTIL = 'callbackList.forEach(function (callbackItem)';
      var readyToUseEvents = JSON.parse(localStorage.getItem(CALENDAR_EVENTS), function(key, value) {

        if (key === CALLBACK && typeof value === "string") {
          if (value.indexOf(KOSTIL) !== -1) {
            return function () {
              console.log('SORRY ZA KOSTIL');
            }
          }
          return eval("(" + value + ")");
        }

        if (key === DATE)
          return new Date(value);
        return value;
      });

      if (readyToUseEvents) {
        readyToUseEvents = getActualEvents(readyToUseEvents);
      }

      return readyToUseEvents;
    }

    /**
     * Find the closest data to event
     * @return {*}
     */
    function findClosestTimeToEvent() {
      var currentTime = Date.now();
      var closestTimeToEvent = null;

      for (var i = 0; i < eventsList.length; i++) {
        var timeToCurrentEvent = eventsList[i].date.getTime();

        if (closestTimeToEvent === null && currentTime < timeToCurrentEvent) {
          closestTimeToEvent = eventsList[i].date.getTime();
        }

        if (currentTime < timeToCurrentEvent && closestTimeToEvent > timeToCurrentEvent) {
          closestTimeToEvent = timeToCurrentEvent;
        }
      }

      return closestTimeToEvent;
    }

    /**
     * Find the Events with same data
     * @param {Number} closestTimeToEvent
     * @return {Array}
     */
    function findEventsWithSameDate(closestTimeToEvent) {
      var closestEventList = [];
      for (var i = 0; i < eventsList.length; i++) {
        var timeToCompare = eventsList[i].date.getTime();
        if (timeToCompare === closestTimeToEvent) {
          closestEventList.push(eventsList[i]);
        }
      }
      return closestEventList;
    }

    /**
     * Find the closest event and start the timer
     */
    function findAndRunClosestEvent() {

      if (timer) {
        clearInterval(timer);
      }

      if (!eventsList.length) {
        return console.error(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
      }

      var currentTime = Date.now();
      var closestTimeToEvent =  findClosestTimeToEvent() || null;

      if (!closestTimeToEvent) {
        return console.log(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
      }

      var closestEventList = findEventsWithSameDate(closestTimeToEvent);

      //calculate the delay and start timer
      var delay = (closestTimeToEvent - currentTime);
      timer = setInterval(function () {
        closestEventList.forEach(function (event) {
          event.callback();
          this.Calendar.removeEvent(event.id);
        });
        findAndRunClosestEvent();
        saveInStorage();
      }, delay);
    }

    /**
     * Clone the object for immutable behavior
     * @param {Object} mainObject
     * @return {Object}
     */
    function cloneObject(mainObject) {
      var objCopy = {}; // objCopy will store a copy of the mainObj
      var key;

      for (key in mainObject) {
        if (mainObject.hasOwnProperty(key)) {
          objCopy[key] = mainObject[key]; // copies each property to the objCopy object
        }
      }

      return objCopy;
    }

    /**
     * Clone the array of objects for immutable behavior
     * @param {Array} listEvents
     * @return {Array}
     */
    function cloneArrayAndObjects(listEvents) {
      var newArrayEvents = [];

      listEvents.forEach(function (object) {
        newArrayEvents.push(cloneObject(object));
      });
      return newArrayEvents;
    }

    /**
     * Get events by month
     * @return {*}
     */
    function getEventsByMonth() {
      var storeEvents = [];
      var currentDate = new Date();
      eventsList.forEach(function(event){
        if (event.date.getMonth() === currentDate.getMonth() &&
          event.date.getFullYear() === currentDate.getFullYear()) {
          storeEvents.push(cloneObject(event));
        }
      });
      return storeEvents;
    }

    /**
     * Get events by week
     * @return {*}
     */
    function getEventsByWeek() {
      var storeEvents = [];
      var currentDate = new Date();
      var startOfAWeek = new Date(currentDate.getFullYear(), currentDate.getMonth(),
        currentDate.getDate() - currentDate.getDay() + 1);
      var endOfAWeek = new Date(currentDate.getFullYear(), currentDate.getMonth(),
        currentDate.getDate() - currentDate.getDay() + 7, 23, 59, 59, 999);

      eventsList.forEach(function (event) {
        if (event.date >= startOfAWeek && event.date <= endOfAWeek) {
          storeEvents.push(cloneObject(event));
        }
      });
      return storeEvents;
    }

    /**
     * Get events by day
     * @param {String} date1
     * @return {*}
     */
    function getEventsByDay(date1) {
      var storeEvents = [];
      var _date1 = new Date(date1);

      eventsList.forEach(function (event) {
        if (event.date.getDate() === _date1.getDate() &&
          event.date.getMonth() === _date1.getMonth() &&
          event.date.getFullYear() === _date1.getFullYear()) {
          storeEvents.push(cloneObject(event));
        }
      });
      return storeEvents;
    }

    /**
     * Get events between dates
     * @param {String} date1
     * @param {String} date2
     * @return {*}
     */
    function getEventsBetweenDates(date1, date2) {
      var storeEvents = [];
      var _date1 = new Date(date1);
      var _date2 = new Date(date2);

      eventsList.forEach(function (event) {
        if (event.date >= _date1 && event.date <= _date2) {
          storeEvents.push(cloneObject(event));
        }
      });
      return storeEvents
    }

    /**
     * Create a new event
     * @param {String} name
     * @param {String} date
     * @param {Function} callback
     * @return {*}
     */
    Calendar.prototype.addEvent = function(name, date, callback){

      if (!name && !date && !callback) {
        return console.error(MESSAGE_INFO_INVALID_OF_INPUT_DATA);
      }

      date = this.validationDate(date);
      if (!date){
        return console.error(MESSAGE_ERROR_FORMAT_DATE);
      }

      var eventDate = new Date(date);
      if (Date.now() - eventDate.getTime() > 0){
        return console.error(MESSAGE_ERROR_THE_DATE_MUST_BE_VALID);
      }

      var isFunction = this.isFunction(callback);

      if (!isFunction) {
        return console.error(MESSAGE_ERROR_CALLBACK_MUST_BE_FUNCTION);
      }

      var event = {
        name: name,
        date: eventDate,
        callback: callback,
        id: generateId()
      };

      eventsList.push(event);
      findAndRunClosestEvent();
      saveInStorage();

      return cloneObject(event);
    };

    /**
     * Show all events
     */
    Calendar.prototype.getAllEvents = function(){
      if (eventsList.length) {
        return cloneArrayAndObjects(eventsList);
      }
      console.error(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
    };

    /**
     * Remove the event by id
     * @param {Number | String} id - unique of Event
     */
    Calendar.prototype.removeEvent = function(id){

      if (!eventsList.length) {
        return console.error(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
      }

      id = this.validationID(id);
      if (!id) {
        console.error(MESSAGE_ERROR_INPUT_OF_ID);
        return  console.log(MESSAGE_ERROR_ID_MUST_BE_NUMBER);
      }

      for (var i = 0; i < eventsList.length; i++) {

        if (eventsList[i].id === id) {
          eventsList.splice(i, 1);
          findAndRunClosestEvent();
          saveInStorage();
          return cloneArrayAndObjects(eventsList);
        }
      }
      console.log(MESSAGE_INFO_EVENT_NOT_FOUND);
    };

    /**
     * Modify the event by id
     * @param {Number | String} id - unique of Event
     * @param {String} newName - unique of Event
     * @param {String} newDate - unique of Event
     * @param {Function} newDate - unique of Event
     */
    Calendar.prototype.modifyEvent = function(id, newName, newDate) {

      id = this.validationID(id);
      if (!id || !newName) {
        console.error(MESSAGE_INFO_INVALID_OF_INPUT_DATA);

        return console.log(MESSAGE_ERROR_ID_MUST_BE_NUMBER);
      }

      if (newDate) {
        newDate = this.validationDate(newDate);

        if (!newDate){
          return console.error(MESSAGE_ERROR_FORMAT_DATE);
        }

        var newObjDate = new Date(newDate);
      }

      for (var i = 0; i < eventsList.length; i++) {

        if (eventsList[i].id === id) {
          eventsList[i].name = newName;
          eventsList[i].date = newDate ? newObjDate : eventsList[i].date;
          eventsList = cloneArrayAndObjects(eventsList);
          findAndRunClosestEvent();
          saveInStorage();

          return cloneObject(eventsList[i]);
        }
      }

      return 'Event with that ID not found!';
    };

    /**
     * Get events by date
     * @param {String} dateIntervalName - Name of flag
     * @param {String} date1 - date
     * @param {String} date2 - date
     */
    Calendar.prototype.getEvents = function(dateIntervalName, date1, date2) {
      var
        BY_MONTH = 'by month',
        BY_DAY = 'by day',
        BY_WEEK = 'by week',
        BETWEEN_DATES = 'between dates';

      if (date1) {
        date1 = this.validationDate(date1);
        if (!date1) {
          return  console.error(MESSAGE_ERROR_FORMAT_DATE);
        }
      }

      if (date2) {
        date2 = this.validationDate(date2);
        if (!date2) {
          return console.error(MESSAGE_ERROR_FORMAT_DATE);
        }
      }

      if (!eventsList.length) {
        return console.log(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
      }

      var validListEvents = [];

      if (dateIntervalName === BY_MONTH) {
        validListEvents = getEventsByMonth();

      } else if (dateIntervalName === BY_WEEK) {

        validListEvents = getEventsByWeek();

      } else if (dateIntervalName === BY_DAY && date1) {

        validListEvents = getEventsByDay(date1);

      } else if (dateIntervalName === BETWEEN_DATES && date1 && date2){

        validListEvents = getEventsBetweenDates(date1, date2);

      } else {
        return console.log(MESSAGE_INFO_OF_FLAGS);
      }

      if (validListEvents.length) {
        return validListEvents;
      }
      console.log(MESSAGE_INFO_EVENTS_LIST_IS_EMPTY);
    };

    Calendar.prototype.validationID = function (id) {
      if (typeof id === 'number') {
        return id;
      }
      return false;
    };

    Calendar.prototype.validationDate = function (date) {
      var parsedDate = Date.parse(date);
      if (isNaN(date) && !isNaN(parsedDate)) {
        return date;
      }
      return false;
    };

    Calendar.prototype.isFunction = function (func) {
      return func && {}.toString.call(func) === '[object Function]';
    };

    return new Calendar();
  })();

  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = Calendar;
  else {
    if (typeof define === 'function' && define.amd) {
      define([], function() {
        return Calendar;
      });
    }
    else {
      window.Calendar = Calendar;
    }
  }
})();